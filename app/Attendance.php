<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    public $timestamps = false;
    protected $table = 'attendances';
    
    public function item(){
    	return $this->belongsTo('App\User');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Permission;
use App\Attendance;
use Illuminate\Support\Facades\Auth;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attendancelist = Attendance::paginate(10);
        return view('welcome')->with('attendancelist',$attendancelist);
    }

    public function show_all()
    {
        $attendances = \DB::table('attendances')->paginate(10);

        return view('attendancelist')->withAttendances($attendances);
    }

    public function my_attendance()
    {
        $user = Auth::user();
        $id = intval($user->id);
        $attendances = Attendance::where('id_number', '=', $id)->paginate(10);

        return view('attendance')->withAttendances($attendances);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $exist = \DB::table('users')->select('id')->where('id', $request->code)->first();
        
        if($exist != ""){
            $attendance = new Attendance();
            $attendance->id_number  = $request->code;
            $attendance->time_in    = \Carbon\Carbon::now();
            $attendance->save();
            return redirect()->route('attendance')->with('alert', 'asd');

        }

        else{
            return redirect()->route('attendance')->with('error', 'asd');
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

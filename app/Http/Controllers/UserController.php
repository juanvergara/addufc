<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Permission;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        $roles = Role::all();

        $userpermissions = Permission::all();

        $tabname = 'users';

        return view('users.users')->withUsers($users)->withRoles($roles)->with('userpermissions', $userpermissions)->withTabname($tabname);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|unique:roles|max:15',
            'userpermissions' =>'required',
            ]
        );

        $role = new Role();

        $role->name         = $request->name;
        $role->description  = $request->description;

        $role->save();

        $permissions = $request['userpermissions'];
        $role = Role::where('name', '=', $request->name)->first();

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail();
            $role->givePermissionTo($p->name);
        }

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role_permissions = Role::with('Permissions')->findOrFail($id);
        $user_permissions = Permission::all();

        return response()->json(['role' => $role_permissions, 'user_permissions' => $user_permissions]);
    } 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($request->input('id'));

        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->status   = $request->status;

        $user->save();

        $role_user = User::has('Roles')->where('id', $request->id)->first();

        if (is_null($role_user)) {

            $user->roles()->attach($request->role_id+1);
        } 

        else {

            $user->roles()->sync($request->role_id+1);
        }

        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function update_role(Request $request)
    {
        $role = Role::find($request->input('id'));;
        $this->validate($request, [
            'name'=>'required|max:15|unique:roles,name,'.$request->input('id'),
            'userpermissions' =>'required',
            ]
        );

        $input = $request->except(['userpermissions']);
        $permissions = $request['userpermissions'];
        $role->fill($input)->save();

        $p_id = Role::with('Permissions')->find($request->input('id'));
        $role = Role::where('name', '=', $request->name)->first();

        foreach ($p_id->permissions as $permission) {
            $p = Permission::where('id', '=', $permission->id)->firstOrFail();
            $role->withdrawPermissionsTo($p->name);
        }

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail();
            $role->givePermissionTo($p->name);
        }

        return redirect()->route('users.index');
    }

    public function profile() {
        $users = User::all();
        $user = Auth::user();
        return view('profile', compact('user'))->withUsers($users);
    }

    public function update_profile(Request $request) {
        $user = User::find($request->input('id'));

        $user->name     = $request->name;
        $user->email    = $request->email;

        $user->save();

        return redirect()->route('profile');
    }

    public function attendance(){
        $users = User::all();
        $user = Auth::user();
        return view('attendance', compact('user'))->withUsers($users);
    }
}
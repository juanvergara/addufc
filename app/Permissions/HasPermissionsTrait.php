<?php

namespace App\Permissions;

use App\{Role, Permission};

trait HasPermissionsTrait
{
    public function givePermissionTo(...$permissions)
    {

        $permissions = $this->getPermissions(array_flatten($permissions));

        if ($permissions === null) {
            return $this;
        }

        $this->permissions()->saveMany($permissions);

        return $this;

    }

    public function withdrawPermissionsTo(...$permissions)
    {

        $permissions = $this->getPermissions(array_flatten($permissions));

        $this->permissions()->detach($permissions);

        return $this;

    }

    public function hasPermissionTo($permission)
    {

        return $this->hasPermissionThroughRoles($permission) || $this->hasPersmission($permission);

    }

    public function hasRole(...$roles)
    {

        foreach ($roles as $role) {

            if ($this->roles->contains('name', $role)) {

                return true;
            }
        }

        return false;

    }

    protected function getPermissions(array $permissions)
    {

        return Permission::whereIn('name', $permissions)->get();

    }

    protected function hasPermissionThroughRoles($permission)
    {

        foreach ($permission->roles as $role) {
            
            if ($this->roles->contains($role)) {

                return true;

            }
        }

        return false;

    }

    protected function hasPersmission($permission)
    {

        return (bool) $this->permissions->where('name', $permission->name)->count();

    }

    public function roles()
    {

        return $this->belongsToMany('App\Role', 'users_roles');

    }

    public function permissions()
    {

        return $this->belongsToMany('App\Permission', 'users_permissions');

    }
}
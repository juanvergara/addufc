<?php

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth'], function() {
	Route::resource('users', 'UserController', ['except' => ['create', 'show', 'destroy']]);
	Route::get('profile', 'UserController@profile')->name('profile');
	Route::post('updateroles', ['uses' => 'UserController@update_role', 'as' => 'users.update_role']);
	Route::any('editprofile', 'UserController@update_profile')->name('editprofile');
	Route::any('myattendance', 'AttendanceController@my_attendance')->name('myattendance');
	Route::any('attendancelist', 'AttendanceController@show_all')->name('attendancelist');
	
});

Route::resource('attendance', 'AttendanceController');
//Route::get('attendancelist', 'AttendanceController@show_all')->name('attendancelist');
Route::get('attendance', 'AttendanceController@index')->name('attendance');


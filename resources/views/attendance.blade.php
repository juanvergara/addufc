@extends('layouts.app')

@section('styles')
    {!! Html::style('css/tabs.css')  !!}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-14">
            <div class="panel with-nav-tabs panel-primary">
                <div class="panel-heading">
                    <h4>Personal Attendance Records</h4>
                </div>

                <center><div class="panel-body">
                    <table class="row table table-hover">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Time In</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($attendances as $attendance)
                            <tr>
                                <td>@php echo date("l, F d, Y", strtotime($attendance->time_in)); @endphp</td>
                                <td>@php echo date("h:i:s A", strtotime($attendance->time_in)); @endphp</td>    
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                     
                </div> </center> 
                <?php echo $attendances->render(); ?> 
            </div>
        </div>
    </div>
</div>
@endsection

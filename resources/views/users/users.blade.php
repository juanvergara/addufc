@extends('layouts.app')

@section('styles')
	{!! Html::style('css/tabs.css')  !!}
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('content')
	<div class="container">
		<h3>User Adminstration</h3>
		<div class="row">
			<div class="col-md-14">
		        <div class="panel with-nav-tabs panel-primary">
		            <div class="panel-heading">
		                    <ul class="nav nav-tabs">
		                        <li class="active"><a href="#users" data-toggle="tab">Users</a></li>
		                        <li><a href="#roles" data-toggle="tab">Roles</a></li>
		                        <li><a href="#permissions" data-toggle="tab">Permissions</a></li>
		                    </ul>
		            </div>
		            <div class="panel-body">
		                <div class="tab-content">
		                    <div class="tab-pane fade in active" id="users">
		                    	@include('users.users_tab')
		                    </div>
		                    <div class="tab-pane fade" id="roles">
		                    	@include('users.roles_tab')
		                    </div>
		                    <div class="tab-pane fade" id="permissions">
		                        @include('users.permissions_tab')
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
	<hr>
@endsection
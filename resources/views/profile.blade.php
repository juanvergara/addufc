@extends('layouts.app')

@section('styles')
	{!! Html::style('css/tabs.css')  !!}
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h4>{{ auth()->user()->name }}'s Profile</h4>
				</div>

				<div class="panel-body">
					<dl class="user-info">
						<br>

            <dt>
              ID Number
            </dt>
            
            <dd>
              @php echo $formattedNumber = sprintf('%03d', auth()->user()->id); @endphp
            </dd>

						<dt>
							Name
						</dt>

						<dd>
							{{ auth()->user()->name }}
						</dd>

						<dt>
							Email
						</dt>
						
						<dd>
							{{ auth()->user()->email }}
						</dd>
			
						<br>

						<center>
							<div class="form-group">
                  <a href="{{ route('myattendance') }}" class="btn btn-primary" style="width:30%">My Attendances</a>  
              </div>

              <div class="form-group">
                <button class="edit-modal btn btn-primary" style="width:30%" data-toggle="modal" data-target="#editUser" data-id="{{$user->id}}" data-name="{{$user->name}}" data-email="{{$user->email}}" data-barcode="{{$user->barcode}}">Edit Profile
							   </button>
							</div>
            </center>
          </dl>
				</div>
			</div>
		</div>
	</div>
</div>

@section('modals')
@parent
 {{-- Edit User Modal --}}
    <div id="editUser" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="panel panel-primary">
              <div class="panel-heading">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="panel-title" id="myModalLabel"><b>Edit User</b></h4>
              </div>
            <div class="modal-body">
              @if(isset($user))
                 {!! Form::model($users, ['route' => ['editprofile', $user->id], 'method' => 'PUT']) !!}
                    {!! Form::hidden('id', null, ['id' => 'uid', 'class' => 'form-control', 'required' => '']) !!}

                    {!! Form::label('name', 'Name:') !!}
                    {!! Form::text('name', null, ['id' => 'un', 'class' => 'form-control', 'required' => '', 'maxlength' => '15']) !!}

                    {!! Form::label('email', 'E-Mail:') !!}
                    {!! Form::text('email', null, ['id' => 'ue', 'class' => 'form-control', 'required' => '', 'maxlength' => '30']) !!}

                    <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal" style="margin-top:20px">Cancel</button>
                      {!! Form::submit('Update', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}
                    </div>
                {!! Form::close() !!}
              @endif
            </div>
          </div>
        </div>
    </div>
@endsection
@endsection

@section('scripts')
@parent
    {{-- Edit User Modal --}}
    <script type="text/javascript">
        $(document).on('click', '.edit-modal', function() {
            $('#uid').val($(this).data('id'));
            $('#un').val($(this).data('name'));
            $('#ue').val($(this).data('email'));
            $('#myModal').modal('show');
        });
    </script>
@endsection


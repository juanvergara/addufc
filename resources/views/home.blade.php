@extends('layouts.app')

@section('content')

<!<div class="container">
<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary ">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    <br>
                    <br>
                    <center>
                        <div class="form-group">
                             <a href="{{ route('profile') }}" class="btn btn-primary" style="width:250px">My Profile</a>  
                        </div>

                        
                        @can('Add User')
                            <div class="form-group">
                                <a href="{{ route('attendancelist') }}" class="btn btn-primary" style="width:250px">Attendance Records</a>
                            </div>
                        @endcan

                    

                        @can('Add User')
                            <div class="form-group">
                                <a href="{{ route('users.index') }}" class="btn btn-primary" style="width:250px">Edit Users</a>
                            </div>
                        @endcan                        
                    </center>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12">
<div class="modal fade" id="attendanceSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="H4">Success</h4>
                </div>
                <div class="modal-body">
                <p class="help-block">Attendance Recorded!</p>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-line btn-rect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="alert" role="alert" id="result"></div>

<div class="col-lg-12">
<div class="modal fade" id="attendanceError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="H4">Error</h4>
                </div>
                <div class="modal-body">
                <p class="help-block">Attendance Already Recorded!</p>
                </div>
                <div class="modal-footer">

                <button type="button" class="btn btn-primary btn-line btn-rect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="alert" role="alert" id="result"></div>


@endsection

@section('scripts')
@parent

@if (session('error'))
    <script type="text/javascript">
        $(document).ready(function(){
        $('#attendanceError').modal('show');
        });
    </script>
@endif

@if (session('success'))
    <script type="text/javascript">
        $(document).ready(function(){
        $('#attendanceSuccess').modal('show');
        });
    </script>
@endif

    <script type="text/javascript">
        var modalConfirm = function(callback){

        $("#btn-confirm").on("click", function(){
        $("#mi-modal").modal('show');
        });

        $("#modal-btn-si").on("click", function(){
        callback(true);
        $("#mi-modal").modal('hide');
        });

        $("#modal-btn-no").on("click", function(){
        callback(false);
        $("#mi-modal").modal('hide');
        });
        };

        modalConfirm(function(confirm){
        if(confirm){
        //Acciones si el usuario confirma
        $("#result").html("CONFIRMADO");
        }else{
        //Acciones si el usuario no confirma
        $("#result").html("NO CONFIRMADO");
        }
        });
    </script>  
@endsection


@section('styles')
    {!! Html::style('css/tabs.css')  !!}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endsection

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {!! Html::style('bootstrap-3.3.7-dist/css/bootstrap.min.css') !!}
        {{--Filter Table CSS --}}
        {!! Html::style('css/filtertable.css')  !!}
        {{--Laravel CSS --}}
        {!! Html::style('css/laravel.css')  !!}

        <!-- for page-specific css -->
        @yield('styles')

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>AdDU Fitness and Wellness Center</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }


            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>

        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    <a href="{{ url('/login') }}">Home</a>
                    <a href="{{ url('/register') }}">Register</a>
                </div>
            @endif

            
            <div class="content">

                <br><br><br><div>
                   <h3> AdDU Fitness and Wellness Center</h3>
                </div>
                    
                <div>
                {!! Form::open(['route' => 'attendance.store',  'method'=>'POST']) !!}
                {!! Form::text('code', null, ['class' => 'form-control']) !!}
                <br>
                {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}
                </div>
                <br>

                @if (session('alert'))
                    <div class="alert alert-success alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong>
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-success alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>User does not exist!</strong>
                    </div>
                @endif

                <div class="container">
                    <div class="row">
                        <div class="col-md-14">
                            <div class="panel with-nav-tabs panel-primary">
                                <div class="panel-heading">
                                    <h4>Attendance List</h4>
                                </div>

                                <center>
                                    <div class="panel-body">
                                        <table class="row table table-hover">
                                            <thead>
                                                <th> ID NUMBER</th>
                                                <th> Timein</th>
                                            </thead>
                                        
                                            <tbody>
                                                @foreach($attendancelist as $student)
                                                    <tr>
                                                        <td>@php echo $formattedNumber = sprintf('%06d', $student->id_number); @endphp</td>
                                                        <td>{{$student->time_in}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </center><?php echo $attendancelist->render(); ?>
                            </div>
                        </div>
                    </div>
                </div>

     
            </div>
        </div>
    </body>
</html>

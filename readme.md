## USERNAME AND PASSWORD

username: admin@mail.com
password: 123456.

## OVERVIEW

ATENEO DE DAVAO UNIVERSITY FITNESS AND WELLNESS CENTER

ATTENDANCE SYSTEM

- attendance in the front page
- login for both the user and the admin
- view profile for both the user and the admin
- the admin view the entire attendance list
- the admin can register/add a user
- the admin can assign roles

Notes:
- The page will be redirected to login page but you can input attendance even without logging in by pressing the title bar above
- Only the admin can register a user in the add user part.
- To logout, press Home in the attendance page and press the user name again to see a drop-down of log-out.

## DEVELOPERS

Alvin John V. Vergara
John Nomer L. Mangrobang